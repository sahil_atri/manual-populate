/**
 * Created by clicklabs49 on 12/6/16.
 */



var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var manualPop = require('./index');

var userSchema = new Schema({
    name: {type: String},
    age: {type: Number}
});
var userModel = mongoose.model('users', userSchema);

var addressSchema = new Schema({
    house: {type: String},
    city: {type: String},
    userid: {type: String}
});

var addressModel = mongoose.model('address', addressSchema);

mongoose.connect('mongodb://localhost/manualpop');
var db = mongoose.connection;


db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("connected to database successfully");
});





// var document = new addressModel({house: "3049", city: "chandigarh", userid: "575d383387b848d140cb75ad"});
// document.save(function (err, result) {
//     console.log(err, result);
// });
var x = [ { __v: 0,
    userid: '575d384436de20e9400fe336',
    city: 'mohali',
    house: '3018',
    },
    { __v: 0,
        userid: '575d383387b848d140cb75ad',
        city: 'chandigarh',
        house: '3049',
    } ];
// addressModel.find({}, {}, {}, function (err, result) {
//     console.log(err, result);
// })

manualPop(x, [
    {
        path: 'userid',
        model: userModel,
        query: {},
        projection: {name: 1}
    }
], function (err, result) {
    console.log(err, result);
});