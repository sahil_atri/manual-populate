## Synopsis

This module populates specified fields of documents from a different mongodb collection when _id of referred document (ObjectId) is stored as hexadecimal string in the referring document field. 

```javascript
Motorparts
{
	_id: ObjectId("575d384436dji4u9400fe336")
}


Car
{
	motorparts : "575d384436dji4u9400fe336" //hexadecimal string value of above
}
```


## More information

With this you do not need to hit database n * k no of times to populate the k fields of n documents, rather only k database hits are used. Deep populate level one (embedded object) is also supported.

Also what happens if referred _id doesn't exist in foreign collection?
Those entries have been made undefined.

## Code Example



1. 

```javascript
var manualPop = require('manual-pop');
var userModel = models.userModel; //mongoose models
var serviceModel = models.serviceModel; //mongoose models

var data = [ 
	{
    		userId: '575d384436de20e9400fe336',
		serviceId: '934d384436de20e9400fkjhu'
    		city: 'mohali',
    		house: '3018',
    	},
    	{
        	userId: '575d383387b848d140cb75ad',
		serviceId: '934d384436de20e9400f12m'
        	city: 'chandigarh',
        	house: '3049',
    	}
];

var populateOptions = [
	{
		path: "userId",
		model: userModel,
		query: {},
		projection: {name: 1}
	},
	{
		path: "serviceId",
		model: serviceModel,
		query: {},
		projection: {serviceName: 1}
	}
]

manualPop(data, populateOptions, function (err, result) {
    console.log(err, result);
});

/*output
	err:- null
	result:- [ 
		{
    			userId: {name: "anchal", _id: "575d384436de20e9400fe336"},
			serviceId: undefined,
    			city: 'mohali',
    			house: '3018',
    		},
    		{
        		userId: {name: "sahil", _id: "575d383387b848d140cb75ad"},
			serviceId: {serviceName: "blackOp", _id: "934d384436de20e9400f12m"},
        		city: 'chandigarh',
        		house: '3049',
    		}
	];
*/
```





## Motivation

The module was created because the project i was part of, stored _id (ObjectId) of referred documents as hexadecimal String in the referring document, making it not possible to use populate (mongoose) or $lookup (mongodb aggregation) functionalities. 
