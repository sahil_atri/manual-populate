/*_____________________________________________________________________________
 |  This function is to be used for populating when normal population fails   |
 |  because you have not used objectId to refer other collections.            |
 |  With this you do not need to hit database n * k no of times to populate   |
 |  the k fields of n documents, rather only k database hits are used.        |
 |  Only deep populate level one is supported.                                         |
 |                                                                            |
 |  Also what happens if referred _id doesn't exist in foreign collection?     |
 |  Those entries have been made undefined.                                   |
 |____________________________________________________________________________*/

var _ = require('underscore');
var async = require('async');

var validatePopulateOptions = function(popOption, callback) {
    if (!popOption.path || !popOption.model || !popOption.query || !popOption.projection) {
        return callback(util.createErrorResponse(400, constants.responseMessage.ERROR_IN_EXECUTION, null, "some fields of populate options missing"))
    }
    callback(null);
};

var createPathHash = function(popOption, data, callback) {
    var pathHash = {};
    async.eachOf(data, function(dataitem, datakey, callback) {
        if (popOption.path.indexOf(".") == -1) {/*incase of normal populate*/
            if (dataitem[popOption.path]) {/*path field present in doc*/
                if (pathHash[dataitem[popOption.path]]) {/*hash already exist*/
                    pathHash[dataitem[popOption.path]].push(dataitem);
                }else {/*new hash*/
                    pathHash[dataitem[popOption.path]] = [dataitem];
                }
            } else {/*path field absent in document*/}
        }else {/*incase of deep level one populate*/
            var firstField = popOption.path.split(".")[0];
            var secondField = popOption.path.split(".")[1];
            if (dataitem[firstField]) {
                if (dataitem[firstField][secondField]) {
                    if (pathHash[dataitem[firstField][secondField]]) {/*hash already exists*/
                        pathHash[dataitem[firstField][secondField]].push(dataitem);
                    }else {/*new hash*/
                        pathHash[dataitem[firstField][secondField]] = [dataitem];
                    }
                }else {/*second field is absent in document*/}
            }else {/*first field is absent in document*/}
        }
        callback(null);
    }, function(err) {
        if (err) {
            console.log(err);
        }
        callback(null, pathHash);
    });
};

module.exports = function(data, populateOptionsArray, callback) {
    if (!data.length) {
        return callback(null, data);
    }
    if (!populateOptionsArray.length) {
        return callback({error: "ERROR_IN_EXECUTION", message: "populate options array not valid"});
    }

    async.eachOf(populateOptionsArray, function(popOption, key, callback){
        async.waterfall([
            function(callback) {/*validate populate options*/
                validatePopulateOptions(popOption, callback);
            },
            function(callback) {/*create hash [key is _id field, value is the document itself]*/
                createPathHash(popOption, data, callback);
            },
            function(hash, callback) {/*get all data refered by _ids*/
                var _ids;
                if (popOption.path.indexOf(".") == -1) {
                    _ids = _.pluck(data, popOption.path);
                }else {
                    var firstField = popOption.path.split(".")[0];
                    var secondField = popOption.path.split(".")[1];
                    _ids = _.map(data, function(elem) {return elem[firstField][secondField]})
                }
                popOption.query["_id"] = {$in: _ids};
                popOption.projection["_id"] = 1;


                popOption.model.find(popOption.query, popOption.projection, {lean: true}, function(err, result) {
                    if (err) {return callback(err);}
                    callback(null, result, hash);
                });
            },
            function(result, hash, callback) {/*append each found data to respective hashed document*/
                if (!result.length) {
                    return callback(null);
                }
                async.eachOf(result, function(foundData, key, callback) {
                    var path = popOption.path;
                    if (popOption.path.indexOf(".") == -1) {
                        for (var i in hash[foundData._id.toString()]) {
                            hash[foundData._id.toString()][i][popOption.path] = foundData;
                        }

                    }else {
                        var firstField = popOption.path.split(".")[0];
                        var secondField = popOption.path.split(".")[1];
                        for (var i in hash[foundData._id.toString()]) {
                            if (hash[foundData._id.toString()][i][firstField]) {
                                if (hash[foundData._id.toString()][i][firstField][secondField]) {
                                    hash[foundData._id.toString()][i][firstField][secondField] = foundData;
                                }
                            }
                        }

                    }
                    callback(null);
                }, function(err) {
                    if (err) {
                        console.log(err);
                    }
                    /*handle those cases where referred document does not exist*/
                    if (popOption.path.indexOf(".") == -1) {
                        for (var i in hash) {
                            for (var j in hash[i]) {
                                if (typeof hash[i][j][popOption.path] == typeof "abc") {
                                    hash[i][j][popOption[path]] = undefined;
                                }
                            }
                            //console.log(typeof hash[i][popOption.path]);

                        }
                    }else {
                        for (var i in hash) {
                            for (var j in hash[i]) {
                                var firstField = popOption.path.split(".")[0];
                                var secondField = popOption.path.split(".")[1];
                                if (hash[i][j][firstField]) {
                                    if (hash[i][j][firstField][secondField]) {
                                        if (typeof hash[i][j][firstField][secondField] == typeof "abc") {
                                            hash[i][j][firstField][secondField] = undefined;
                                        }
                                    }
                                }
                            }

                        }


                    }
                    callback(null);
                })
            }
        ], function(err, result) {
            callback(err);
        });
    }, function(err) {
        if (err) {
            return callback(err);
        }
        callback(null, data);
    })
};
