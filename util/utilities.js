exports.createErrorResponse = function(statusCode, message, data, details) {
    return {
        response: {
            message: message,
            data: data,
            details: details
        },
        statusCode: statusCode
    }
}


exports.createSuccessResponse = function(statusCode, message, data) {
    return {
        response: {
            message: message,
            data: data
        },
        statusCode: statusCode
    }
}

